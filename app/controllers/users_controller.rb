class UsersController < ApplicationController
  
  def index
   @search = params[:search]  
    @users = User.search(@search) 
  end

  def show
  	@user = User.find(params[:id])
  	@posts = @user.posts
  end

  def add_friend
    
  end
  
  def remove_friend
    
  end

end
