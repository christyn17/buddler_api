class User < ActiveRecord::Base

	has_many :friendships
  has_many :friends, :through => :friendships
	has_many :inverse_friendships, :class_name => "Friendship", :foreign_key => "friend_id"
  has_many :inverse_friends, :through => :inverse_friendships, :source => :user

	has_one :profile
	has_many :posts
	has_many :comments
	has_attached_file :image, styles: { large: "600x600", medium: "300x300#", thumb: "100x100#"}
	validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  attr_accessible :email, :password, :password_confirmation, :remember_me, :firstname, :lastname, :image

  def self.search(search)
    if search
       @users = User.where('firstname LIKE ?', "%#{@search}%")
     
    else
      find(:all)
    end
  end


 

def active?
  !!active
end

def inactive_message
  "Sorry, this account has been deactivated."
end

def soft_delete
  # assuming you have deleted_at column added already
  update_attribute(:deleted_at, Time.current)
  update_attribute(:active, false)

end

end
